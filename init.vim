set nocompatible

filetype plugin on

let mapleader = " "

set rnu
set nu

set lcs=eol:$,tab:»·
set list

set scrolloff=5

set wildmenu
set ruler

set ignorecase
set smartcase
set nohlsearch

set lazyredraw

syntax on

set path+=**

set tabstop=8
set shiftwidth=0
set smarttab
set autoindent
set smartindent
set copyindent

set wrap

if has('gui_running')
	colorscheme desert
else
	colorscheme pablo
endif

set splitbelow
set splitright

set backspace=indent,eol,start

set mouse=a

set fileformat=unix
set fileformats=unix,dos

set encoding=utf-8

" use primary selection (aka. "*)as clipboard
set clipboard=unnamed

set colorcolumn=80

fun! HeaderGuard()
	let basename = toupper( substitute(bufname(""), ".*/", "", ""))
	let guard = substitute(basename, "[ \.]", "_", "g")
	call append(0, "#ifndef " . guard)
	call append(1, "#define " . guard . " 1")
	call append(2, "")
	call append(line("$"), "")
	call append(line("$"), "#endif /* " . guard . " */")
endfun
map <leader>g :call HeaderGuard()<CR>

autocmd BufNewFile *.h call HeaderGuard()

autocmd BufRead,BufNewFile mkfile set filetype=make
autocmd BufRead,BufNewFile .clang-format set filetype=yaml
autocmd BufNewFile *.h call HeaderGuard()
autocmd BufRead,BufNewFile README set filetype=markdown
autocmd BufRead,BufNewFile tmac.* set filetype=nroff

autocmd FileType python set colorcolumn+=73 expandtab tabstop=4
autocmd FileType markdown set spell
autocmd FileType cs set expandtab tabstop=4

let g:netrw_liststyle=3
let g:netrw_banner=0

map <Up> ""
map! <Up> <Esc>
map <Down> ""
map! <Down> <Esc>
map <Left> ""
map! <Left> <Esc>
map <Right> ""
map! <Right> <Esc>

highlight CursorLine cterm=NONE ctermbg=242
highlight CursorColumn cterm=NONE ctermbg=242
map <leader>c :se cul! cuc!<CR>
